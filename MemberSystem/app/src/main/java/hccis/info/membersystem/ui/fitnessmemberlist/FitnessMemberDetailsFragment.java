package hccis.info.membersystem.ui.fitnessmemberlist;


import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.provider.ContactsContract;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.google.gson.Gson;

import hccis.info.membersystem.MainActivity;
import hccis.info.membersystem.R;
import hccis.info.membersystem.entity.Member;
import hccis.info.membersystem.jpa.MemberJPA;
import hccis.info.membersystem.jpa.UserJPA;

/**
 * A simple {@link Fragment} subclass.
 */
public class FitnessMemberDetailsFragment extends Fragment {

    private UserJPA user;
    private MemberJPA member;
    private TextView textViewName, textViewUserId, textViewUserType, textViewUserEmail, textViewUserCreatedDate, textViewPhoneCell, textViewAddress;
    private Button btnAddToContact, btnSendEmail;

    public FitnessMemberDetailsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

            if (getArguments() != null) {
                String camperJson = getArguments().getString("user");
                Gson gson = new Gson();
                user = gson.fromJson(camperJson, UserJPA.class);
            }

        if (getArguments() != null) {
            String camperJson = getArguments().getString("member");
            Gson gson = new Gson();
            member = gson.fromJson(camperJson, MemberJPA.class);
        }

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_fitness_member_details, container, false);

        /** Save button related **/
        btnAddToContact = root.findViewById(R.id.btnSaveContact);
        btnAddToContact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ContactsContract.Intents.Insert.ACTION);
                intent.setType(ContactsContract.RawContacts.CONTENT_TYPE);

                /** Append information from object to be saved in contacts **/
                intent.putExtra(ContactsContract.Intents.Insert.NAME, user.getFirstName());

                /** Home Number **/
                intent.putExtra(ContactsContract.Intents.Insert.PHONE, member.getPhoneHome());
                intent.putExtra(ContactsContract.Intents.Insert.PHONE_TYPE, ContactsContract.CommonDataKinds.Phone.TYPE_HOME);

                /** Mobile Number **/
                intent.putExtra(ContactsContract.Intents.Insert.SECONDARY_PHONE, member.getPhoneCell());
                intent.putExtra(ContactsContract.Intents.Insert.SECONDARY_PHONE_TYPE, ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE);

                /** Work Number **/
                intent.putExtra(ContactsContract.Intents.Insert.TERTIARY_PHONE, member.getPhoneWork());
                intent.putExtra(ContactsContract.Intents.Insert.TERTIARY_PHONE_TYPE, ContactsContract.CommonDataKinds.Phone.TYPE_WORK);

                /** Work Email **/
                intent.putExtra(ContactsContract.Intents.Insert.EMAIL, user.getUsername());
                intent.putExtra(ContactsContract.Intents.Insert.EMAIL_TYPE, ContactsContract.CommonDataKinds.Email.TYPE_WORK);

                /** Include a note in contact **/
                intent.putExtra(ContactsContract.Intents.Insert.NOTES, "Saved by Member System");

                /** Show contacts application **/
                startActivity(intent);
            }
        });

        /** Member full name **/
        textViewName = root.findViewById(R.id.textView_detailed_fragment_user_full_name);
        textViewName.setText(user.getFirstName() + " " + user.getLastName());

        /** Member user id **/
        textViewUserId = root.findViewById(R.id.textView_detailed_fragment_user_id);
        textViewUserId.setText(user.getUserId().toString());

        /** Member user type **/
        textViewUserType = root.findViewById(R.id.textView_detailed_fragment_user_type);

        /** This will reformat the output to readable String **/
        switch (user.getUserTypeCode().toString()){
            case "1":
                textViewUserType.setText("Admin");
                break;
            case "2":
                textViewUserType.setText("Member");
                break;
            default:
                Log.d("asheikho", "No type found");
        }

        /** Member email **/
        textViewUserEmail = root.findViewById(R.id.textView_detailed_fragment_contact_email);
        textViewUserEmail.setText(user.getUsername());

        /** Member phone cell **/
        textViewAddress = root.findViewById(R.id.textView_detailed_fragment_contact_address);
        textViewAddress.setText(member.getAddress());

        /** Member address**/
        textViewPhoneCell = root.findViewById(R.id.textView_detailed_fragment_contact_phone_cell);
        textViewPhoneCell.setText(member.getPhoneCell());

        /** Member account created date **/
        textViewUserCreatedDate = root.findViewById(R.id.textView_detailed_fragment_created_date);
        textViewUserCreatedDate.setText(user.getCreatedDateTime());

        btnSendEmail = root.findViewById(R.id.btnSendEmail);
        btnSendEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent email = new Intent(Intent.ACTION_SEND);
                email.putExtra(Intent.EXTRA_EMAIL, user.getUsername());
                email.putExtra(Intent.EXTRA_SUBJECT, "Member System notification");
                email.putExtra(Intent.EXTRA_TEXT, "Hello, This is an email from member system!");

                //need this to prompts email client only
                email.setType("message/rfc822");
                startActivity(Intent.createChooser(email, "Choose an Email client :"));
            }
        });

        return root;
    }

}
