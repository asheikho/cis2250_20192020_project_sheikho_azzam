package hccis.info.membersystem.ui.home;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.synnapps.carouselview.CarouselView;
import com.synnapps.carouselview.ImageClickListener;
import com.synnapps.carouselview.ImageListener;

import hccis.info.membersystem.MainActivity;
import hccis.info.membersystem.R;

public class HomeFragment extends Fragment {

    private HomeViewModel homeViewModel;

    /** Images to be displayed in carousel view **/
    private int[] mImages = new int[] {
            R.drawable.preview1, R.drawable.preview2, R.drawable.preview3, R.drawable.preview4
    };

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        homeViewModel =
                ViewModelProviders.of(this).get(HomeViewModel.class);
        View root = inflater.inflate(R.layout.fragment_home, container, false);


        /** Find Carousel View **/
        CarouselView carouselView = root.findViewById(R.id.imageContainer);

        /** Page Count **/
        carouselView.setPageCount(mImages.length);

        /** Set images for view **/
        carouselView.setImageListener(new ImageListener() {
            @Override
            public void setImageForPosition(int position, ImageView imageView) {
                imageView.setImageResource(mImages[position]);
            }
        });


        return root;
    }
}