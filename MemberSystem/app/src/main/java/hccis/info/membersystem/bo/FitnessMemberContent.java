package hccis.info.membersystem.bo;

import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import hccis.info.membersystem.jpa.MemberJPA;
import hccis.info.membersystem.util.JsonAPI;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class FitnessMemberContent {

    /** Items to be displayed **/

    public static List<MemberJPA> loadFitnessMembersService(){

        final List<MemberJPA> fitnessMembersList = new ArrayList<>();

        Log.d("asheikho","LOADING Fitness Members");

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://hccis.info:8080/court/rest/MemberService/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        JsonAPI jsonApi = retrofit.create(JsonAPI.class);
        Call<List<MemberJPA>> call = jsonApi.getMembers();


        call.enqueue(new Callback<List<MemberJPA>>() {
            @Override
            public void onResponse(Call<List<MemberJPA>> call, Response<List<MemberJPA>> response) {
                if(!response.isSuccessful()){
                    //getTextViewResult().setText("Code" + response.code());
                    Log.d("asheikho", "" + response.code());

                    return;
                }

                List<MemberJPA> members = response.body();

                /** empty array list to prevent data duplication on output **/
                if(fitnessMembersList.size() >= 1) {

                    fitnessMembersList.removeAll(fitnessMembersList);
                }

                for (MemberJPA member: members){

                    fitnessMembersList.add(member);
                }

            }

            @Override
            public void onFailure(Call<List<MemberJPA>> call, Throwable t) {
                Log.d("asheikho", t.getMessage());
            }
        });


        return fitnessMembersList;
    }
}
