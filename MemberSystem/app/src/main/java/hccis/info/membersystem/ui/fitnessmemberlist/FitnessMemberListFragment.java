package hccis.info.membersystem.ui.fitnessmemberlist;

import androidx.core.app.NotificationCompat;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.ViewModelProviders;

import android.app.Activity;
import android.app.NotificationManager;
import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import hccis.info.membersystem.MainActivity;
import hccis.info.membersystem.R;
import hccis.info.membersystem.bo.FitnessMemberContent;
import hccis.info.membersystem.jpa.MemberJPA;
import hccis.info.membersystem.jpa.UserJPA;
import hccis.info.membersystem.util.JsonAPI;
import hccis.info.membersystem.util.MemberApplication;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class FitnessMemberListFragment extends Fragment {

    /** Items to be displayed **/
    private static List<String> fitnessMemberItems = new ArrayList<>();
    private List<UserJPA> usersArr = new ArrayList<>();
    private List<MemberJPA> membersArr = FitnessMemberContent.loadFitnessMembersService();

    public FitnessMemberListFragment(){
        // Required empty public constructor

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        /** Inflate the FitnessMemberList **/
        View view = inflater.inflate(R.layout.fragment_fitness_member_list, container, false);

        /** Look for are ListView within the layout by id **/
        final ListView listView = (ListView) view.findViewById(R.id.fitnessMemberListView);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://hccis.info:8080/court/rest/UserAccessService/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        JsonAPI jsonApi = retrofit.create(JsonAPI.class);
        Call<List<UserJPA>> call = jsonApi.getUsers();

        call.enqueue(new Callback<List<UserJPA>>() {
            @Override
            public void onResponse(Call<List<UserJPA>> call, Response<List<UserJPA>> response) {
                if(!response.isSuccessful()){
                    //getTextViewResult().setText("Code" + response.code());
                    Log.d("asheikho", "" + response.code());

                    return;
                }

                List<UserJPA> users = response.body();

                /** empty array list to prevent data duplication on output **/
                if(fitnessMemberItems.size() >= 1) {
                    fitnessMemberItems.removeAll(fitnessMemberItems);
                }

                /** Appends user id + first, last name to be viewed in list view as items **/
                for (UserJPA user: users){
                    String content = "";
                    content += user.getUserId() + " — " + user.getFirstName() + " " + user.getLastName();
                    fitnessMemberItems.add(content);
                }

                /** Append UserJPA objects to be sent to request method in main activity **/
                for(int i = 0; i < fitnessMemberItems.size(); i++){
                    usersArr.add(users.get(i));
                }

                /** Create an adapter to convert the array to be displayable by list view **/
                ArrayAdapter<String> listViewAdapter = new ArrayAdapter<String>(
                        getActivity(),
                        android.R.layout.simple_list_item_1,
                        fitnessMemberItems
                );

                /** set the adapter to use the list view we created **/
                listView.setAdapter(listViewAdapter);


                /** Enable click listener for each item **/
                listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                        for(int i = 0; i < fitnessMemberItems.size(); i++){
                            if(position == i){
                                /** Send objects to handle the request **/
                                MainActivity.handleRequest(usersArr.get(i), membersArr.get(i));

                                /** Indicate to user that loaded the item **/
                                Toast.makeText(getActivity(), "Loaded Successfully", Toast.LENGTH_SHORT).show();

                            }
                        }
                    }
                });

                /** notify the user about loading the members **/
                sendNotification();
            }

            @Override
            public void onFailure(Call<List<UserJPA>> call, Throwable t) {
                Log.d("asheikho", t.getMessage());
            }
        });



        return view;

    }

    /** creates a notification to be requested later**/
    private synchronized void sendNotification() {
        //Channel Id is ignored on lower APIs
        NotificationCompat.Builder builder =
                new NotificationCompat.Builder(MemberApplication.getContext(), MemberApplication.MEMBER_CHANNEL_ID)
                        .setSmallIcon(R.drawable.logo)
                        .setContentTitle("JSON Notification")
                        .setContentText("Done downloading Members");

        NotificationManager notificationManager = (NotificationManager) MemberApplication.getContext().getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(0, builder.build());
    }
}
