package hccis.info.membersystem.ui.qrscan;

import androidx.lifecycle.ViewModelProviders;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.google.zxing.integration.android.IntentIntegrator;

import hccis.info.membersystem.R;

/**
 * asheikho
 * 17 Feb 2020
 * This fragment is responsible to handle the QR scanner
 * **/
public class QrScanFragment extends Fragment {

    private QrScanViewModel qrScanViewModel;
    private Button btnScan;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        qrScanViewModel =
                ViewModelProviders.of(this).get(QrScanViewModel.class);
        View root = inflater.inflate(R.layout.fragment_members, container, false);

        btnScan = root.findViewById(R.id.btnScan);

        /** When the scan button is clicked, show the QR scanner **/
        btnScan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // This is a simple way to invoke barcode scanning and receive the result
                IntentIntegrator intentIntegrator = new IntentIntegrator(getActivity());
                intentIntegrator.setDesiredBarcodeFormats(IntentIntegrator.QR_CODE_TYPES);
                intentIntegrator.setPrompt("Scan");

                // Use a specific camera of the device
                intentIntegrator.setCameraId(0);
                intentIntegrator.setBeepEnabled(false);
                intentIntegrator.setBarcodeImageEnabled(false);

                // Initiates a scan for all known barcode types with the default camera.
                intentIntegrator.initiateScan();
            }
        });

        return root;

    }

}
