package hccis.info.membersystem.util;

import java.util.List;

import hccis.info.membersystem.jpa.MemberJPA;
import hccis.info.membersystem.jpa.UserJPA;
import retrofit2.Call;
import retrofit2.http.GET;

public interface JsonAPI {

    @GET("member")
    Call<List<MemberJPA>> getMembers();

    @GET("user")
    Call<List<UserJPA>> getUsers();

}
